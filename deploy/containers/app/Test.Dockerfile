#=========================================================================
ARG FROM_BASE
FROM $FROM_BASE as base
#=========================================================================

#=========================================================================
FROM base as composer

RUN apt-get update \
# install deps
    && apt-get install -y --no-install-recommends git ssh zip unzip wget \
    && apt-get purge \
        -y --auto-remove \
        -o APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

#composer
COPY ./deploy/containers/app/composer-install.sh ./composer-install.sh
RUN chmod +x ./composer-install.sh \
    && ./composer-install.sh \
    && mv composer.phar /usr/local/bin/composer \
    && chmod +x /usr/local/bin/composer
#=========================================================================

#=========================================================================
FROM composer as test

#phpdbg
#IMPORTANT set the same version for phpdbg in Base.DockerFile
COPY --from=php:8-cli /usr/local/bin/phpdbg /usr/local/bin/

# Install extensions
#xdebug
RUN  pecl install xdebug

COPY ./deploy/containers/app/xdebug.sh /usr/local/bin/xdebug
RUN chmod +x /usr/local/bin/xdebug
#=========================================================================

#=========================================================================
FROM test as develop

COPY ./deploy/containers/app/php-debug.ini /usr/local/etc/php/php.ini
# Enable extensions
#RUN docker-php-ext-enable xdebug
#=========================================================================

