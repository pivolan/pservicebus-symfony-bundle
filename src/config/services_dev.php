<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $p = $containerConfigurator->parameters();
    $p->set('p-service-bus.bus.error_handler_default_set', false);
};
