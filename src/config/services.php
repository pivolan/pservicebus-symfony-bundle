<?php

declare(strict_types=1);

use GDXbsv\PServiceBus\Bus;
use GDXbsv\PServiceBus\Bus\Handling\OnlyOnce\NotControlling;
use GDXbsv\PServiceBus\Bus\Handling\OnlyOnce\OnlyOnceControl;
use GDXbsv\PServiceBus\Bus\Handling\OnlyOnce\OnlyOnceInit;
use GDXbsv\PServiceBus\Bus\Handling\OnlyOnce\OnlyOnceInitConsoleCommand;
use GDXbsv\PServiceBus\Bus\ServiceBus;
use GDXbsv\PServiceBus\InitConsoleCommand;
use GDXbsv\PServiceBus\Message\Replay\ReplayConsoleCommand;
use GDXbsv\PServiceBus\Message\Replay\Replaying;
use GDXbsv\PServiceBus\Saga\InMemoryPersistence;
use GDXbsv\PServiceBus\Saga\SagaFindDefinitions;
use GDXbsv\PServiceBus\Saga\SagaHandling;
use GDXbsv\PServiceBus\Saga\SagaMapper;
use GDXbsv\PServiceBus\Saga\SagaPersistence;
use GDXbsv\PServiceBus\Serializer\ClojureSerializer;
use GDXbsv\PServiceBus\Serializer\Serializer;
use GDXbsv\PServiceBus\Transport\ConsumeConsoleCommand;
use GDXbsv\PServiceBus\Transport\TransportSyncConsoleCommand;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\DependencyInjection\Reference;

use function Symfony\Component\DependencyInjection\Loader\Configurator\tagged_iterator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $p = $containerConfigurator->parameters();
    $p->set('p-service-bus.bus.error_handler_default_set', true);


    $s = $containerConfigurator->services();

    $s->defaults()
        ->autowire()
        ->autoconfigure()
        ->private();

    $s->set(ServiceBus::class);
    $s->alias(Bus::class, ServiceBus::class);
    $s->alias(Bus\ConsumeBus::class, ServiceBus::class);
    $s->alias(Bus\CoroutineBus::class, ServiceBus::class);

    $s->set(ClojureSerializer::class);
    $s->alias(Serializer::class, ClojureSerializer::class);

    $s->set(SagaHandling::class);
    $s->set(SagaMapper::class);
    $s->alias(SagaFindDefinitions::class, SagaMapper::class);
    $s->set(InMemoryPersistence::class)->call('setCoroutineBus', [new Reference(Bus\CoroutineBus::class)]);
    $s->alias(SagaPersistence::class, InMemoryPersistence::class);

    $s->set(NotControlling::class);
    $s->alias(OnlyOnceControl::class, NotControlling::class);
    $s->alias(OnlyOnceInit::class, NotControlling::class);

    $s->set(Replaying::class);


    $s->set(TransportSyncConsoleCommand::class);
    $s->set(OnlyOnceInitConsoleCommand::class);
    $s->set(ConsumeConsoleCommand::class);
    $s->set(InitConsoleCommand::class);
    $s->set(ReplayConsoleCommand::class);
};
