<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusBundle\DependencyInjection;

use GDXbsv\PServiceBus\Bus\Middleware\InMiddleware;
use GDXbsv\PServiceBus\Bus\Middleware\OutMiddleware;
use GDXbsv\PServiceBus\Bus\ServiceBus;
use GDXbsv\PServiceBus\Transport\ConsumeConsoleCommand;
use GDXbsv\PServiceBus\Transport\Transport;
use GDXbsv\PServiceBus\Transport\TransportSynchronisation;
use Symfony\Component\Config\Exception\FileLocatorFileNotFoundException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension;

/**
 * @internal
 */
final class PServiceBusExtension extends ConfigurableExtension
{
    public function loadInternal(array $mergedConfig, ContainerBuilder $container)
    {
        $loader = new PhpFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../config')
        );
        $loader->load('services.php');
        $env = $container->getParameter('kernel.environment');
        try {
            $loader->load("services_{$env}.php");
        } catch (FileLocatorFileNotFoundException $e) {
            //ignore if no file for env
        }
        $this->transports($mergedConfig, $container);

        $container
            ->registerForAutoconfiguration(TransportSynchronisation::class)
            ->addTag('p-service-bus.transport.sync')
            ->setLazy(true);

        $this->loadMiddlewares($container, $mergedConfig);
    }


    /**
     * @return array<string, Transport>
     */
    protected function transports(array $mergedConfig, ContainerBuilder $container): void
    {
        $transports = [];

        foreach ($mergedConfig['transports'] as $name => $serviceId) {
            $transports[$name] = new Reference($serviceId);
        }

        $serviceBus = $container->getDefinition(ServiceBus::class);
        $serviceBus->setArgument('$transportMap', $transports);

        $consumeConsoleCommand = $container->getDefinition(ConsumeConsoleCommand::class);
        $consumeConsoleCommand->setArgument('$transports', $transports);

        $container->setParameter('p-service_bus.temp.transports', $transports);
    }

    private function loadMiddlewares(ContainerBuilder $container, array $mergedConfig)
    {
        $container
            ->registerForAutoconfiguration(InMiddleware::class)
            ->addTag('service_bus.middleware.in');
        $container
            ->registerForAutoconfiguration(OutMiddleware::class)
            ->addTag('service_bus.middleware.out');
    }
}
